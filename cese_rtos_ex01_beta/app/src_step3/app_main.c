/*
 * Copyright (c) 2022 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : app_main.c
 * @date   : May 1, 2022
 * @author : Sebastian Bedin <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "utils.h"
#include "msgbuffer.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/********************** macros and definitions *******************************/

#define BUTTON_TIME_MAX_VALUE_          (9999)
#define BUTTON_MSG_MAX_LEN_             (10) // "TECx Tyyyy" = 10

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static xQueueHandle g_queue_h_;
static char const * const g_str_led_on_ = "LED ON";
static char const * const g_str_button_format_ = "TEC%d T%d";
static msgbuffer_t g_msgbuffer_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void a_led_message_routine(bool enable)
{
  if(false == enable)
  {
    return;
  }

  EX_DEBUG_CONSOLE("[a] Led ON\r\n");

  size_t str_msg_len = strlen(g_str_led_on_);

  void *p_message = msgbuffer_sender_message_create(&g_msgbuffer_, str_msg_len + 1);
  {
    if(NULL != p_message)
    {
      strncpy((char*)p_message, g_str_led_on_, str_msg_len + 1);
    }
  }
  msgbuffer_sender_post(&g_msgbuffer_, p_message, 0);
}

static void task_sender_a_(void *p_parameters)
{
  led_init();

  while (true)
  {
    led_toglle();

    a_led_message_routine(led_read());

    vTaskDelay(1000 / portTICK_RATE_MS);
  }
}

static size_t b_message_generate(char *str_msg_buffer, unsigned int const button_id)
{
  uint32_t time = button_press_time(button_id);
  EX_DEBUG_CONSOLE("[b] Button:%u, Time:%u\r\n", button_id, time);

  if(BUTTON_TIME_MAX_VALUE_ < time)
  {
    time = BUTTON_TIME_MAX_VALUE_;
  }
  snprintf(str_msg_buffer, BUTTON_MSG_MAX_LEN_ + 1, g_str_button_format_, button_id, time);

  return strlen(str_msg_buffer);
}

static void b_button_time_rutine(unsigned int const button_id)
{
  char str_msg_buffer[BUTTON_MSG_MAX_LEN_ + 1];
  size_t str_msg_len = b_message_generate(str_msg_buffer, button_id);

  void *p_message = msgbuffer_sender_message_create(&g_msgbuffer_, str_msg_len + 1);
  {
    if(NULL != p_message)
    {
      strncpy((char*)p_message, str_msg_buffer, str_msg_len + 1);
    }
  }
  msgbuffer_sender_post(&g_msgbuffer_, p_message, 0);
}

static void task_sender_b_(void *p_parameters)
{
  buttons_init();

  while (true)
  {
    b_button_time_rutine(0);
    b_button_time_rutine(1);

    vTaskDelay(1000 / portTICK_RATE_MS);
  }
}

static void task_receiver_c_(void *p_parameters)
{
  while (true)
  {
    void *p_message = msgbuffer_receiver_get(&g_msgbuffer_, portMAX_DELAY);
    {
      if(NULL != p_message)
      {
        EX_DEBUG_CONSOLE("[c] msg: %s\r\n", p_message);
      }
    }
    msgbuffer_receiver_message_destroid(&g_msgbuffer_, p_message);
  }
}

/********************** external functions definition ************************/

int app(void)
{
  bool msgbuffer_status = msgbuffer_init(&g_msgbuffer_, 5);
  while(!msgbuffer_status)
  {
    // problem !
  }

  BaseType_t state;

  state = xTaskCreate(task_sender_a_, "task_sender_a", configMINIMAL_STACK_SIZE, NULL, 2, NULL);
  while (pdPASS != state)
  {
    // problem !
  }

  state = xTaskCreate(task_sender_b_, "task_sender_b", configMINIMAL_STACK_SIZE * 2, NULL, 2, NULL);
  while (pdPASS != state)
  {
    // problem !
  }

  state = xTaskCreate(task_receiver_c_, "task_receiver_c", configMINIMAL_STACK_SIZE, NULL, 1, NULL);
  while (pdPASS != state)
  {
    // problem !
  }

  vTaskStartScheduler();

  while (true)
  {
    // problem !
  }

  return 1;
}

/********************** end of file ******************************************/
