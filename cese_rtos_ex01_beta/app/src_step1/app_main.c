/*
 * Copyright (c) 2022 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : app_main.c
 * @date   : May 1, 2022
 * @author : Sebastian Bedin <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "utils.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/********************** macros and definitions *******************************/

#define BUTTON_TIME_MAX_VALUE_          (9999)
#define BUTTON_MSG_MAX_LEN_             (10) // "TECx Tyyyy" = 10

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static xQueueHandle g_queue_h_;
static char const * const g_str_led_on_ = "LED ON";
static char const * const g_str_button_format_ = "TEC%d T%d";

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void task_sender_a_(void *p_parameters)
{
  led_init();

  while (true)
  {
    led_toglle();

    if(true == led_read())
    {
      EX_DEBUG_CONSOLE("[a] Led ON\r\n");

      size_t str_msg_len = strlen(g_str_led_on_);
      char *str_msg = (char*)pvPortMalloc(str_msg_len + 1);
      if(NULL != str_msg)
      {
        strncpy(str_msg, g_str_led_on_, str_msg_len + 1);

        portBASE_TYPE status;
        status = xQueueSendToBack(g_queue_h_, (void*)&str_msg, 0);
        if (pdPASS == status)
        {
          EX_DEBUG_CONSOLE("[a] Send: %s\r\n", str_msg);
        }
      }
    }

    vTaskDelay(1000 / portTICK_RATE_MS);
  }
}

static void task_sender_b_(void *p_parameters)
{
  char str_msg_buffer[BUTTON_MSG_MAX_LEN_ + 1];

  buttons_init();

  while (true)
  {
    for(unsigned int button_id = 0; button_id < 2; ++button_id)
    {
      uint32_t time = button_press_time(button_id);
      EX_DEBUG_CONSOLE("[b] Button:%u, Time:%u\r\n", button_id, time);

      if(BUTTON_TIME_MAX_VALUE_ < time)
      {
        time = BUTTON_TIME_MAX_VALUE_;
      }
      snprintf(str_msg_buffer, BUTTON_MSG_MAX_LEN_ + 1, g_str_button_format_, button_id, time);

      size_t str_msg_len = strlen(str_msg_buffer);
      char* str_msg = (char*)pvPortMalloc(str_msg_len + 1);
      if(NULL != str_msg)
      {
        strncpy(str_msg, str_msg_buffer, str_msg_len + 1);

        portBASE_TYPE status;
        status = xQueueSendToBack(g_queue_h_, (void*)&str_msg, 0);
        if (pdPASS == status)
        {
          EX_DEBUG_CONSOLE("[b] Send: %s\r\n", str_msg);
        }
      }
    }

    vTaskDelay(1000 / portTICK_RATE_MS);
  }
}

static void task_receiver_c_(void *p_parameters)
{
  portBASE_TYPE status;
  char const *str_msg = NULL;

  while (true)
  {
    status = xQueueReceive(g_queue_h_, (void*)&str_msg, portMAX_DELAY);
    if (pdPASS == status)
    {
      EX_DEBUG_CONSOLE("[c] Receive: %s\r\n", str_msg);
      vPortFree((void*)str_msg);
      str_msg = NULL;
    }
  }
}

/********************** external functions definition ************************/

int app(void)
{
  g_queue_h_ = xQueueCreate(5, sizeof(int32_t));
  while (NULL == g_queue_h_)
  {
    // problem !
  }

  BaseType_t state;

  state = xTaskCreate(task_sender_a_, "task_sender_a", configMINIMAL_STACK_SIZE, NULL, 2, NULL);
  while (pdPASS != state)
  {
    // problem !
  }

  state = xTaskCreate(task_sender_b_, "task_sender_b", configMINIMAL_STACK_SIZE, NULL, 2, NULL);
  while (pdPASS != state)
  {
    // problem !
  }

  state = xTaskCreate(task_receiver_c_, "task_receiver_c", configMINIMAL_STACK_SIZE, NULL, 1, NULL);
  while (pdPASS != state)
  {
    // problem !
  }

  vTaskStartScheduler();

  while (true)
  {
    // problem !
  }

  return 1;
}

/********************** end of file ******************************************/
