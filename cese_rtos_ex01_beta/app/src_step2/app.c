
#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define OS_DEBUG_CONSOLE_(...)\
  taskENTER_CRITICAL();\
  {\
    DEBUGOUT(__VA_ARGS__);\
  }\
  taskEXIT_CRITICAL()

xQueueHandle xQueue;

static bool led_state_;

static void led_init_(void)
{
  led_state_ = false;
}

static void led_toglle_(void)
{
  led_state_ = !led_state_;
}

static bool led_read_(void)
{
  return led_state_;
}

#define MSG_LEN_ON              ("LED ON")

static void vTaskA(void *pvParameters)
{
  portBASE_TYPE xStatus;
  int32_t value = 1;
  const portTickType xDelay = 1000 / portTICK_RATE_MS;

  led_init_();

  while (true)
  {
    OS_DEBUG_CONSOLE_("[A] Loop Start\r\n");
    led_toglle_();

    if(true == led_read_())
    {
      OS_DEBUG_CONSOLE_("[A] Led ON\r\n");
      size_t str_msg_len = strlen(MSG_LEN_ON);
      char* str_msg = (char*)pvPortMalloc(str_msg_len + 1);
      strncpy(str_msg, MSG_LEN_ON, str_msg_len + 1);

      xStatus = xQueueSendToBack(xQueue, (void*)&str_msg, 0);
      if (xStatus == pdPASS)
      {
        OS_DEBUG_CONSOLE_("[A] Message Send\r\n");
      }
    }

    OS_DEBUG_CONSOLE_("[A] Loop End\r\n");
    vTaskDelay(xDelay);
  }
}

#define MSG_BUTTON_ON              ("TIEMPO X")

static void vTaskB(void *pvParameters)
{
  portBASE_TYPE xStatus;
  int32_t value = 2;
  const portTickType xDelay = 2000 / portTICK_RATE_MS;

  while (true)
  {
    OS_DEBUG_CONSOLE_("[B] Loop Start\r\n");

    size_t str_msg_len = strlen(MSG_BUTTON_ON);
    char* str_msg = (char*)pvPortMalloc(str_msg_len + 1);
    strncpy(str_msg, MSG_BUTTON_ON, str_msg_len + 1);

    xStatus = xQueueSendToBack(xQueue, (void*)&str_msg, 0);
    if (xStatus == pdPASS)
    {
      OS_DEBUG_CONSOLE_("[B] Message Send\r\n");
    }

    OS_DEBUG_CONSOLE_("[B] Loop End\r\n");
    vTaskDelay(xDelay);
  }
}

static void vTaskC(void *pvParameters)
{
  int32_t lReceivedValue;
  portBASE_TYPE xStatus;
//  const TickType_t xTicksToWait = pdMS_TO_TICKS(100UL);

  char* str_msg;
  while (true)
  {
    xStatus = xQueueReceive(xQueue, (void*)&str_msg, portMAX_DELAY);

    if (xStatus == pdPASS)
    {
//      vPrintStringAndNumber("Received = ", lReceivedValue);
      OS_DEBUG_CONSOLE_("[C] msg: %s\r\n", str_msg);
      vPortFree((void*)str_msg);
    }
    else
    {
      OS_DEBUG_CONSOLE_("[C] WTF\r\n");
    }
  }
}

int app(void)
{
//  xQueue = xQueueCreate(5, sizeof(int32_t));
  xQueue = xQueueCreate(5, sizeof(void*));

  xTaskCreate(vTaskA, "TaskA", configMINIMAL_STACK_SIZE, NULL, 2, NULL);
  xTaskCreate(vTaskB, "TaskB", configMINIMAL_STACK_SIZE, NULL, 2, NULL);
  xTaskCreate(vTaskC, "TaskC", configMINIMAL_STACK_SIZE, NULL, 1, NULL);

  vTaskStartScheduler();

  while (true)
  {
  }

  return 1;
}

/**
 * @}
 */
